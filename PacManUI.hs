{-# LANGUAGE BangPatterns     #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE PatternGuards    #-}

-- PacMan Graphical UI, inspired from gloss-conway example.
import Control.Monad
import Control.Applicative
import qualified Control.Concurrent
import qualified Control.Concurrent.STM as STM
import qualified Data.Vector as Vec
import Data.List ((\\))
import Data.Maybe
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import System.Random
import qualified System.Environment
import PacMan
import System.IO
import Debug.Trace

import qualified PacParser

data World
        = World
        { worldState            :: PacState

        -- | Width and height of each cell.
        , worldCellSize         :: Int

        -- | Number of pixels to leave between each cell.
        , worldCellSpace        :: Int

        -- | Seconds to wait between each simulation step.
        , worldSimulationPeriod :: Float

        -- | Time that has elapsed since we drew the last step
        , worldElapsedTime      :: Float }

-- | Proxy function between world and game state
worldCells :: World -> Vec Cell
worldCells =  mazeCells . maze . worldState
worldWidth :: World -> Int
worldWidth = mazeWidth . maze . worldState
worldHeight :: World -> Int
worldHeight = mazeHeight . maze . worldState
worldPacman :: World -> PacMan
worldPacman = pacman . worldState
worldGhosts :: World -> [Ghost]
worldGhosts= ghosts . worldState

instance PacWorld World where
    width = worldWidth
    height = worldHeight

grey :: Color
grey = greyN 0.5

ghostColor Character { name = "Blinky" } = red
ghostColor Character { name = "Pinky" } = rose
ghostColor Character { name = "Inky" } = cyan
ghostColor Character { name = "Clyde" } = orange

-- | The basic shape of a cell.
cellShape :: Int -> Int -> Int -> Picture
cellShape cellSize posXi posYi
 = let  cs      = fromIntegral cellSize
        posX    = fromIntegral posXi
        posY    = fromIntegral posYi
        x1      = posX
        x2      = posX + cs
        y1      = posY
        y2      = posY + cs
   in   Polygon [(x1, y1), (x1, y2), (x2, y2), (x2, y1)]


-- | Convert a cell to a picture, based on a primitive shape.
--      We pass the shape in to avoid recomputing it for each cell.
pictureOfCell :: Int -> Int -> Int -> Cell -> Picture
pictureOfCell cellSize posX posY cell
 = case cell of
        Free           -> Color grey                              (cellShape cellSize posX posY)
        Dot            -> picutureOfDot                           cellSize posX posY
        Wall           -> Color black                             (cellShape cellSize posX posY)

picutureOfDot cellSize posXi posYi =
    Color magenta $ Translate (posX + 20) (posY + 20)
                                  $ ThickCircle 2.0 5.0
    where posX = fromIntegral posXi
          posY = fromIntegral posYi


pictureOfPacman cellSize posXi posYi =
    Color yellow $ Translate (posX + 20) (posY + 20)
                $ Scale 5 5
                $ ThickCircle 0 5.0
    where posX = fromIntegral posXi
          posY = fromIntegral posYi

--pictureOfPacman cellSize posX posY = Pictures [color $ Translate (fromIntegral posX) (fromIntegral posY) $ Circle 5.0]
--    where color = Color (makeColor 255 255 0 1.0)
pictureOfGhost ghost cellSize posXi posYi  =
    Color (ghostColor ghost) $ Translate (posX + 20) (posY + 20)
                $ Scale 6 6
                $ ThickCircle 0 5.0
    where posX = fromIntegral posXi
          posY = fromIntegral posYi

pacWorld :: PacState -> IO World
pacWorld pacstate = return $ World
                             { worldState = pacstate
                             , worldCellSize = 35
                             , worldCellSpace = 0
                             , worldSimulationPeriod = 0.35
                             , worldElapsedTime = 0 }


-- | Get the cell at a particular coordinate in the world.
getCell :: World -> Coord -> Cell
getCell world coord@(x, y)
        | x < 0 || x >= worldWidth  world       = Wall
        | y < 0 || y >= worldHeight world       = Wall

        | otherwise
        = worldCells world Vec.! indexOfCoord world coord


-- | Get the neighbourhood of cells around this coordinate.
getNeighbourhood :: World -> Coord -> [Cell]
getNeighbourhood world (ix, iy)
 = let  indexes = [ (x, y)
                        | x <- [ix - 1 .. ix + 1]
                        , y <- [iy - 1 .. iy + 1]
                        , not (x == ix && y == iy) ]
   in   map (getCell world) indexes

stepWorld :: [GhostVar] -> PacStateVar -> LivesVar -> World -> IO World
stepWorld ghostVars psVar livesVar world =
    do let
       STM.atomically $ STM.swapTMVar psVar $ worldState world
       maybeGhosts <- STM.atomically $ mapM STM.tryReadTMVar ghostVars
       lives <- STM.atomically $ STM.takeTMVar livesVar
       STM.atomically $ STM.putTMVar livesVar 0
       let rLives = (remainingLives $ worldState world) - lives
       let justGhosts = catMaybes maybeGhosts
       let movedGhosts = mergeGhosts justGhosts (ghosts $ worldState world)
       let updatedState = computeCatch (worldState world) { ghosts = movedGhosts
                                                          , remainingLives = rLives }
       return $ world { worldElapsedTime = 0
                      , worldState = updatedState }

-- | Simulation function for worlds.
simulateWorld :: [GhostVar] -> PacStateVar -> LivesVar -> Float -> World -> IO World
simulateWorld ghostVars psVar livesVar time world
        -- If enough time has passed then it's time to step the world.
        | worldElapsedTime world >= (worldSimulationPeriod world)
        = stepWorld ghostVars psVar livesVar world
        -- Wait some more.
        | otherwise
        = return $ world { worldElapsedTime = worldElapsedTime world + time }


handleEvent :: Event -> World -> IO World
handleEvent event world
    | EventKey (Char 'w') Down _ _ <- event = let dir = North   in return $ world { worldState = (movePacmanEvent dir) }
    | EventKey (Char 's') Down _ _ <- event = let dir = South   in return $ world { worldState = (movePacmanEvent dir) }
    | EventKey (Char 'a') Down _ _ <- event = let dir = West    in return $ world { worldState = (movePacmanEvent dir) }
    | EventKey (Char 'd') Down _ _ <- event = let dir = East    in return $ world { worldState = (movePacmanEvent dir) }
    | EventKey (Char 'q') Down _ _ <- event = do
        handleGameEvent Save
        return world
    | otherwise = return $ world
    where movePacmanEvent = movePacman (worldState world)
          handleGameEvent = gameEvent (worldState world)

-- | Convert a world to a picture.
drawWorld :: World -> IO Picture
drawWorld world
 = let  (windowWidth, windowHeight) = windowSizeOfWorld world
        offsetX = - fromIntegral windowWidth  / 2
        offsetY = - fromIntegral windowHeight / 2
        stateContent = gameState $ worldState world
        worldPicture = Translate offsetX offsetY
                                 $ Pictures
                                 $ Vec.toList
                                 $ Vec.imap (drawCell world) (worldCells world)
   in   return $ Pictures [worldPicture, Translate offsetX (-offsetY - 30)
                                                   $ Scale 0.2 0.2
                                                   $ Text stateContent ]

gameOver :: World -> IO Picture
gameOver world = return $ Translate ((- fromIntegral windowWidth  / 2) + 60) 0.0 $ Text "GAME OVER!"
    where (windowWidth, windowHeight) = windowSizeOfWorld world

gameWon :: World -> IO Picture
gameWon world = return $ Translate ((- fromIntegral windowWidth  / 2) + 150) 0.0 $ Text "You won!"
    where (windowWidth, windowHeight) = windowSizeOfWorld world

type DrawFunction = World -> IO Picture
drawIf :: Maybe DrawFunction -> (Bool, DrawFunction) -> Maybe DrawFunction
drawIf (Just drawFunction) _ = Just drawFunction
drawIf Nothing (cond, f) = if cond then Just f else Nothing

drawGame :: World -> IO Picture
drawGame world = do
    let pacstate = worldState world
    let rDots = remainingDots pacstate
    let rLives = remainingLives pacstate
    let Just drawFunction = foldl drawIf Nothing [(rLives <= 0, gameOver), (rDots <= 0, gameWon), (True, drawWorld)]
    drawFunction world

-- | Convert a cell at a particular coordinate to a picture.
drawCell :: World -> Index -> Cell -> Picture
drawCell world index cell
   | isPacmanPosition pacstate (x, y) = pictureOfPacman cellSize fx fy
   | isGhostPosition pacstate (x, y)  = pictureOfGhost (ghostAt pacstate (x, y)) cellSize fx fy
   | otherwise                        = pictureOfCell cellSize fx fy cell
   where  cs       = fromIntegral (worldCellSize world)
          cp       = fromIntegral (worldCellSpace world)
          (x, y)   = coordOfIndex world index
          fx       = fromIntegral x * (cs + cp) + 1
          fy       = fromIntegral y * (cs + cp) + 1
          cellSize = worldCellSize world
          pacstate = worldState world

-- | Get the size of the window needed to display a world.
windowSizeOfWorld :: World -> (Int, Int)
windowSizeOfWorld world
 = let  cellSize        = worldCellSize world
        cellSpace       = worldCellSpace world
        cellPad         = cellSize + cellSpace
        height          = cellPad * (worldHeight world) + cellSpace
        width           = cellPad * (worldWidth  world) + cellSpace
   in   (width, height + 50)



-- | Threads and applications

type GhostVar = STM.TMVar Ghost
type PacStateVar = STM.TMVar PacState
type MotionVar = STM.TMVar Motion
type LivesVar = STM.TMVar Int

-- | State of the agreement between ghosts for the mouvements
data Motion = Motion { motionGhosts :: [Ghost] } deriving (Show)

-- | Resolve conflict for a ghost, taking motions of the other ghosts into consideration
-- | Conflict resolution is made random for efficiency
resolveConflit :: PacState -> Ghost -> [Direction] -> Direction -> Motion -> IO Direction
resolveConflit _ _ _ strat Motion { motionGhosts = [] } = do return strat
resolveConflit pacstate ghost excludeStrat strat motion =
    if conflict
    then do newStrat <- randomFrom $ (listDirection \\ excludeStrat)
            resolveConflit pacstate ghost (strat:excludeStrat) newStrat motion
    else return strat
    where conflict = (position movedGhost) `elem` (map position $ motionGhosts motion)
          movedGhost = move (maze pacstate) (go strat $ ghost)

ghostThread :: Int -> Float -> PacStateVar -> LivesVar -> MotionVar -> GhostVar -> IO ()
ghostThread  step delayInSeconds psVar livesVar motionVar ghostVar = do
    Control.Concurrent.threadDelay $ round $ delayInSeconds * 1000000
    pacstate <- STM.atomically $ STM.readTMVar psVar
    ghost <- STM.atomically $ STM.readTMVar ghostVar
    let ghostPos = position ghost
    let pacmanPos = position $ pacman pacstate
    let stratFunction = getStrat $ name ghost
    -- retrieve strategy before resolving potential conflict
    chosenStrat <- stratFunction step ghostPos pacstate
    currentMotion <- STM.atomically $ STM.takeTMVar motionVar
    -- | Remove previous ghost motion
    let cleanedGhosts = removeElem ghost $ motionGhosts currentMotion
    let motion = currentMotion { motionGhosts = cleanedGhosts }
    strat <- resolveConflit pacstate ghost [] chosenStrat motion
    let (movedGhost, killed) = catchPacman pacstate $ move (maze pacstate) (go strat $ ghost)
    let updatedMotion = motion { motionGhosts = movedGhost:(motionGhosts motion)}
    lives <- STM.atomically $ STM.takeTMVar livesVar
    STM.atomically $ STM.putTMVar livesVar (lives + killed)
    STM.atomically $ STM.putTMVar motionVar updatedMotion
    STM.atomically $ STM.swapTMVar ghostVar movedGhost
    let updatedStep = if step > (toggleStratCounter * 2) then 0 else step + 1
    ghostThread updatedStep delayInSeconds psVar livesVar motionVar ghostVar

main :: IO ()
main = do
    [filePath] <- System.Environment.getArgs
    content <- readFile filePath
    let pacstate = convert filePath $ PacParser.pacparse content
    ghostVars <- Control.Monad.mapM STM.newTMVarIO $ ghosts pacstate
    psVar <- STM.newTMVarIO pacstate
    livesVar <- STM.newTMVarIO 0
    motionVar <- STM.newTMVarIO $ Motion []
    world <- pacWorld pacstate
    let delay = worldSimulationPeriod world
    mapM_ (Control.Concurrent.forkIO) (map (ghostThread 0 delay psVar livesVar motionVar) ghostVars)
    playIO (InWindow "PacMan" (windowSizeOfWorld world) (5, 5))
           grey 10 world drawGame handleEvent (simulateWorld ghostVars psVar livesVar)
