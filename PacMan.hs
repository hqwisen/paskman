module PacMan where

import Control.Monad.State
import qualified Data.Map as M
import Data.List.Split (splitOn)
import Data.List (intercalate)
import Data.Maybe
import Data.Char (toLower)
import qualified Data.Vector as Vec
import qualified PacParser
import qualified Maskell
import System.Random
import System.Directory
import qualified System.Environment
import Debug.Trace

type Vec        = Vec.Vector
type Index      = Int
type GhostName = String

-- | The x (col) y (row) coordinate of a cell.
type Coord      = (Int, Int)

class PacWorld w where
    width :: w -> Int
    height :: w -> Int
    indexOfCoord :: w -> Coord -> Index
    indexOfCoord world (x, y) = x + y * (width world)
    coordOfIndex :: w -> Index -> Coord
    coordOfIndex world i = ( i `mod` width world
                           , i `div` width world)


data PacState = PacState
                { sourceFile :: FilePath
                , maze :: Maze
                , remainingLives :: Int
                , remainingDots :: Int
                , pacman :: PacMan
                , ghosts :: [Ghost] } deriving (Show)

instance PacWorld PacState where
    width ps  = width $ maze ps
    height ps = height $ maze ps

instance PacWorld Maze where
    width m = mazeWidth m
    height m = mazeHeight m

data Direction = North | South | East | West deriving (Eq, Enum, Bounded, Ord, Read, Show)

data Cell = Wall | Free | Dot deriving (Eq)

instance Show Cell where
    show Wall = "Wall"
    show Free = "Free absent"
    show Dot = "Free present"

data Maze = Maze
            { mazeCells :: Vec Cell
            , mazeWidth :: Int
            , mazeHeight :: Int } deriving (Show)

data Character = Character
                { name :: String
                , position :: Position
                , direction :: Direction } deriving (Show)

instance Eq Character where
    a == b = (name a) == (name b)

data GameEvent = Save deriving (Show)

type Position = Coord
type PacMan = Character
type Ghost = Character
type StratFunction = Int -> Position -> PacState -> IO Direction

-- | Maze to Maskell (Shortest path impl. of assign #1) Format

convertCM :: Cell -> Char
convertCM Wall = 'X'
convertCM _ = ' '

convertPM :: Maze -> Maskell.Maze
convertPM maze =
    let rlength = width maze
        cells = Vec.toList $ mazeCells maze
    in iconcat rlength $ map convertCM cells


-- | Strategies

-- | Counter that toggle strategies of a ghost, to allow more mouvements
toggleStratCounter  :: Int
toggleStratCounter =  15

-- | Strats:
--      Blinky : kill pacman
--      Inky   : Guard left side
--      Pinky  : Guard bottom side
--      Clyde  : Guard right side
strats :: [(GhostName, StratFunction)]
strats = [("Blinky", killPacmanStrat), ("Inky", guardSide (1, 11))
         , ("Pinky", guardSide (13, 1)), ("Clyde", guardSide (25, 19))]
getStrat :: GhostName -> StratFunction
getStrat ghostName =
    let stratsMap = M.fromList strats
    in case  M.lookup ghostName stratsMap of Just strat -> strat
                                             -- Default: Kill Pacman!
                                             Nothing -> killPacmanStrat

guardSide :: Position -> StratFunction
guardSide guardPos step source pacstate =
    let pacmanPos = position $ pacman pacstate
    in if step < toggleStratCounter
    then do shortestPathStrat source guardPos pacstate
    else do shortestPathStrat source pacmanPos pacstate


shortestPathStratWithMaskell :: Position -> Position -> PacState -> IO Direction
shortestPathStratWithMaskell source@(sc, sr) dest@(dc, dr) pacstate =
    let
        -- NOTE Maskell is very slow, use strat fromFile
        -- Use shortest path algorithm from project #1, to return the best next step
        -- Maskell uses (row, col) style, where we used (col, row) for PacMan
        (_, prev) = Maskell.dijkstra (dr, dc) $ convertPM $ maze pacstate
        (cr, cc) =  Maskell.mapGet (sr, sc) prev
    in return $ posToDir source (cc, cr)

shortestPathStratFromFile' :: FilePath -> Position -> Position -> PacState -> IO Direction
shortestPathStratFromFile' fileName source@(sc, sr) dest@(dc, dr) pacstate = do
    prevStr <- readFile fileName
    let prev = read prevStr :: Maskell.PrevMap
    -- Maskell uses (row, col) style, where we used (col, row) for PacMan
    let (closestRow, closestCol) =  Maskell.mapGet (sr, sc) prev
    return $ posToDir source (closestCol, closestRow)

shortestPathStratFromFile :: Position -> Position -> PacState -> IO Direction
shortestPathStratFromFile source@(sc, sr) dest@(dc, dr) pacstate = do
    let pacmanIndex = indexOfCoord pacstate dest
    let fileName = "quickStrat/" ++ (show pacmanIndex) ++ ".txt"
    exist <- doesFileExist fileName
    if exist
    then shortestPathStratFromFile' fileName source dest pacstate
    else shortestPathStratWithMaskell source dest pacstate

shortestPathStrat :: Position -> Position -> PacState -> IO Direction
shortestPathStrat = do shortestPathStratFromFile

killPacmanStrat :: StratFunction
killPacmanStrat step ghostPos pacstate =
    let pacmanPos = position $ pacman pacstate
    in shortestPathStrat ghostPos pacmanPos pacstate

randomStrat :: IO Direction
randomStrat = do randomFrom listDirection

randomFrom :: [a] -> IO a
randomFrom list = do let l = length list - 1
                     randomIndex <- randomRIO (0, l)
                     return $ list !! randomIndex

-- | Utils functions

listDirection :: [Direction]
listDirection = enumFrom (minBound :: Direction)

posToDir :: Position -> Position -> Direction
posToDir source oneStepDest
    | oneStepDest == (addTuple source $ cellMove North) = North
    | oneStepDest == (addTuple source $ cellMove South) = South
    | oneStepDest == (addTuple source $ cellMove East) = East
    | oneStepDest == (addTuple source $ cellMove West) = West
    -- Return North by default (arbitrary)
--    | otherwise = error "Cannot get direction from positions."
    | otherwise = North

-- Inverse concat
iconcat :: Int -> [a] -> [[a]]
iconcat _ [] = []
iconcat rlength list =
    let (row, remaining) = splitAt rlength list
    in row:(iconcat rlength remaining)

replaceAt :: Int -> a -> Vec a -> Vec a
replaceAt index value ls =
    let (prefix, suffix) = Vec.splitAt index ls
    in (Vec.++) prefix $ Vec.cons value (Vec.tail suffix)

replaceAtL :: Int -> a -> [a] -> [a]
replaceAtL index value ls =
    let (prefix, suffix) = splitAt index ls
    in (++) prefix $ value:(tail suffix)

removeElem :: (Eq a) => a -> [a] -> [a]
removeElem _ [] = []
removeElem e (l:ls)
    | e == l = removeElem e ls
    | otherwise = l : removeElem e ls

isDot :: Cell -> Bool
isDot c = c == Dot

parsedDotToCell :: PacParser.PacDot -> Cell
parsedDotToCell pdot = case pdot of PacParser.Dot -> Dot
                                    PacParser.NoDot -> Free

cellMove :: Direction -> (Int, Int)
cellMove direction = case direction of North -> (0, 1)
                                       South -> (0, -1)
                                       East  -> (1, 0)
                                       West  -> (-1, 0)

addTuple :: (Num a) => (a, a) -> (a, a) -> (a, a)
addTuple (a, b) (c, d) = (a+c, b+d)

pacmanPosition :: PacState -> Position
pacmanPosition ps = position $ pacman ps

ghostPositions :: PacState -> [Position]
ghostPositions ps = map position $ ghosts ps

isPacmanPosition :: PacState -> Position -> Bool
isPacmanPosition ps pos = (==) pos $ pacmanPosition ps

isGhostPosition :: PacState -> Position -> Bool
isGhostPosition ps pos = pos `elem` ghostPositions ps

ghostAt :: PacState -> Position -> Ghost
ghostAt pacstate pos = (filter (\ghost -> pos == position ghost) $ ghosts pacstate) !! 0

isValidPosition :: Position -> Maze -> Bool
isValidPosition pos maze = let index = indexOfCoord maze pos
                               cells = mazeCells maze
                           in cells Vec.! index /= Wall

-- | Merge ghosts set. Ghosts from the first list are prefered.

mergeGhosts :: [Ghost] -> [Ghost] -> [Ghost]
mergeGhosts gs0 gs1 = gs0 ++ filteredGs1
    where filteredGs1 = catMaybes $ map filtr gs1
          filtr = (\g -> if g `elem` gs0
                         then Nothing
                         else Just g)

-- | Game play

go :: Direction -> Character -> Character
go newdir character = character { direction = newdir }

move :: Maze -> Character -> Character
move maze character
    | isValidPosition newpos maze = character { position = newpos }
    | otherwise = character
    where newpos = addTuple currentpos $ cellMove $ direction character
          currentpos = position character

gameEvent :: PacState -> GameEvent -> IO PacState
gameEvent pacstate Save = do
    saveState pacstate
    return pacstate

gameState :: PacState -> String
gameState ps = "Lives: " ++ lives ++ "  Remaining dots: " ++ dots
    where lives = show $ remainingLives ps
          dots = show $ remainingDots ps

centralPostion :: PacState -> Position
centralPostion pacstate =
    let w = mazeWidth $ maze pacstate
        h = mazeHeight $ maze pacstate
    in (w `div` 2, h `div` 2)

catchPacman :: PacState -> Ghost -> (Ghost, Int)
catchPacman pacstate ghost =
    if (position ghost) == pacmanPos
    then (ghost { position = centralPostion pacstate
               , direction = North }, 1)
    else (ghost, 0)
    where pacmanPos = position $ pacman pacstate

computeCatch :: PacState -> PacState
computeCatch pacstate =
    pacstate { remainingLives = lives }
    where pacmanPos = position $ pacman pacstate
          lives = (remainingLives pacstate) - updateLives
          updateLives = if pacmanPos `elem` (map position $ ghosts pacstate)
                        then 1 else 0


pointOf :: Cell -> Int
pointOf Dot = 1
pointOf _ = 0

computePoint:: PacState -> PacState
computePoint pacstate =
    pacstate { remainingDots = updatedDots
             , maze = updatedMaze }
    where updatedDots = (remainingDots pacstate) - eatDot
          updatedCell = case cell of Dot -> Free
                                     _ -> cell
          updatedMaze = (maze pacstate) { mazeCells = updatedCells }
          pacmanIndex = indexOfCoord psMaze $ position $ pacman pacstate
          psMaze = maze pacstate
          cells = mazeCells psMaze
          updatedCells = replaceAt pacmanIndex updatedCell cells
          cell = cells Vec.! pacmanIndex
          eatDot = pointOf cell

movePacman :: PacState -> Direction -> PacState
movePacman pacstate newdir =
    computePoint pacstate { pacman = movedPacman }
    where movedPacman = move (maze pacstate) (go newdir $ pacman pacstate)
-- | Parsing and PacState conversion

-- | Converting parsed game state to PacState datatype

computeCell :: Coord -> PacParser.Cell -> PacState -> (Cell, PacState)
computeCell _ PacParser.Wall pacstate =  (Wall, pacstate)

computeCell _ (PacParser.Free pdot) pacstate =
    (cell, newstate)
    where newstate = pacstate
                     { remainingDots = (+ remainingDots pacstate) $ if isDot cell then 1 else 0 }
          cell = parsedDotToCell pdot

computeCell coord (PacParser.PacMan direction) pacstate =
    (Free, newstate)
    where newstate = pacstate { pacman = character }
          character = (pacman pacstate)
                      { position = coord
                      , direction = read (show direction) :: Direction }

computeCell coord (PacParser.Ghost name pdot) pacstate =
    (cell, newstate)
    where newstate = pacstate
                     { ghosts = ghost:(ghosts pacstate)
                     , remainingDots = (+ remainingDots pacstate) $ if isDot cell then 1 else 0 }
          cell = parsedDotToCell pdot
          ghost = Character
                  { name = show name
                  , position = coord
                  , direction = North }


computeState ::  Int -> PacParser.Cell -> PacState -> (Cell, PacState)
computeState index pcell pacstate =
    (cell, newstate)
    where newmaze = (maze pacstate) { mazeCells = Vec.snoc (mazeCells $ maze pacstate) cell }
          coord = coordOfIndex pacstate index
          (cell, computedstate) = computeCell coord pcell pacstate
          newstate = computedstate { maze = newmaze }

addCell :: Int -> PacParser.Cell -> State PacState Cell
addCell index pcell = state $ computeState index pcell

-- | Inner recursive conversion function
convertI :: PacState -> Int -> [PacParser.Cell] -> PacState
convertI currentState _ [] = currentState
convertI currentState index (pcell:pcells) =
    convertI newState (index+1) pcells
    where (result, newState) = runState (addCell index pcell) currentState

convert :: FilePath -> PacParser.State -> PacState
convert filePath (PacParser.State lives rows) =
    convertI initialState 0 $ concat $ reverse rows
    where initialState = PacState
                        { sourceFile = filePath
                        , maze = Maze (Vec.fromList []) width height
                        , remainingLives = length lives
                        , remainingDots = 0
                        , pacman = initialPacman
                        , ghosts = [] }
          width = length $ rows !! 0
          height = length rows
          initialPacman = Character
                          { name = "PacMan"
                          , position = (0, 0)
                          , direction = North }

-- | Write PacState to disk functions

writeRow :: Maze -> Int -> [String]
writeRow maze rowNumber = map show row
    where row = map (cells Vec.!) indices
          coords = map (\c -> (c, rowNumber)) (enumFromTo 0 $ (mazeWidth maze) - 1)
          indices = map (indexOfCoord maze) coords
          cells = mazeCells maze

writeGhost :: Maze -> [String] -> Ghost -> [String]
writeGhost maze content ghost =
    replaceAtL index value content
    where value = "Ghost " ++ (ghostName) ++ " " ++ dot
          ghostName = map toLower $ name ghost
          index = (indexOfCoord maze $ position ghost)
          currentValue = content !! index
          dot = (splitOn " " currentValue) !! 1

writePacman :: Maze -> [String] -> PacMan -> [String]
writePacman maze content pacman =
    replaceAtL index value content
    where value = "Pacman " ++ pacmanDir
          pacmanDir = map toLower $ show $ direction pacman
          index = (indexOfCoord maze $ position pacman)

saveState :: PacState -> IO ()
saveState pacstate = do writeFile fileName contentStr
    where fileName = sourceFile pacstate
          mazeContent = concat $ map (writeRow psMaze) (enumFromTo 0 $ height - 1)
          withGhosts = foldl (writeGhost psMaze) mazeContent $ ghosts pacstate
          width = mazeWidth psMaze
          height = mazeHeight psMaze
          lives = replicate (remainingLives pacstate) "PacLife"
          content = iconcat width $ writePacman psMaze withGhosts $ pacman pacstate
          contentStr = intercalate "\n" $ map (intercalate " ") $ lives:(reverse content)
          psMaze = maze pacstate
          cells = mazeCells $ psMaze

-- | Test function, to retrieve pacstate, usually within ghci

test :: IO PacState
test = do
          content <- readFile "pacman.pac"
          let pacstate = convert "pacman.pac" $ PacParser.pacparse content
          return pacstate


-- | Strat storage function
-- | Maskell is slow, so we'll store the dijkstra output on disk to improve perf.

generateDijkstra :: PacState -> IO ()
generateDijkstra pacstate = do
    putStrLn ("Computing shortest path for " ++ (show $ length indexToCompute) ++ " cells.")
    mapM writeSolution indexToCompute
    return ()
    where x = 1
          indexToCompute = filter (\i -> cells Vec.! i  /= Wall) $ enumFromTo 0 $ length cells - 1
          cells = mazeCells $ maze pacstate
          maskellMaze = convertPM $ maze pacstate
          fileName index = "quickStrat/" ++ (show index) ++ ".txt"
          posOf index = coordOfIndex pacstate index
          writeSolution index = trace("Running " ++ show index)(writeFile (fileName index) $ show $ runDijkstra $ posOf index)
          -- Maskell uses (row, col) style, where we used (col, row) for PacMan
          runDijkstra (c, r) = let (_, prev) = Maskell.dijkstra (r, c) maskellMaze
                            in prev
