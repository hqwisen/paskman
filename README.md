# PacMan

Functional Programming Assignment #2: PacMan in Haskell.

## Remark

`Maskell.hs` is the shortest path implementation of the first assignment. 
Because the algorithm is poorly implemented
i.e. (very) slow, the shortest path output are stored
in `quickStrat` folder and are used to compute the ghosts strategies.

## Files

- `PacParser.hs`: Parser to retrieve the content of a `.pac` file
- `Maskell.hs`: First assignment implementation (used for shortest path computation)
- `PacMan.hs`: Contains the logic of the game
- `PacManUI.hs`: Graphical interface implementation
- `Parser.hs`: Parser from the practicals
- `newgame.pac`: Initial maze
- `quickStrat`: Maskell output for shortest path

## Game implementation

Implemented in *real-time* i.e. the ghosts move automatically.

Strategies:

 - Blinky: Follow pacman to kill him
 - Inky   : Guard left side of the map
 - Pinky  : Guard bottom side of the map
 - Clyde  : Guard right side of the map

## Compile

```
ghc PacManUI.hs
```

## Create a new game

```
cp newgame.pac game.pac
```

## Play

**Note:** `quickStrat` folder is required to run `PacManUI`.

**Control**: `w, a, s, d` to move Pacman and `q` to save the game state.

```
./PacManUI game.pac
```