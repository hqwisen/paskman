module PacParser where

import qualified System.IO
import qualified Control.Monad
import qualified Parser

data Direction = North | South | East | West deriving (Show)
data PacDot = Dot | NoDot deriving (Show)

data State = State [Life] [Row] deriving (Show)
data Life = Life deriving (Show)
data Cell = Wall
          | PacMan Direction
          | Ghost GhostName PacDot
          | Free PacDot deriving (Show)
data GhostName = Blinky | Pinky | Inky | Clyde deriving (Show)

type Row = [Cell]
type Rows = [Row]
type Lives = [Life]

direction :: Parser.Parser Direction
direction = Parser.space >>  Parser.oneof [Parser.string "north" >> return North,
                                           Parser.string "south" >> return South,
                                           Parser.string "east" >> return East,
                                           Parser.string "west" >> return West]
pacdot :: Parser.Parser PacDot
pacdot =  Parser.space >> Parser.oneof [Parser.string "present" >> return Dot,
                                        Parser.string "absent" >> return NoDot]
ghostname :: Parser.Parser GhostName
ghostname = Parser.space >> Parser.oneof [Parser.string "blinky" >> return Blinky,
                                          Parser.string "pinky" >> return Pinky,
                                          Parser.string "inky" >> return Inky,
                                          Parser.string "clyde" >> return Clyde]



wall = Parser.string "Wall" >> return Wall
pacman = Parser.string "Pacman" >> Control.Monad.liftM PacMan direction

free :: Parser.Parser Cell
free = Parser.string "Free" >> Control.Monad.liftM Free pacdot

ghost :: Parser.Parser Cell
ghost = Parser.string "Ghost" >> Control.Monad.liftM2 Ghost ghostname pacdot

cell :: Parser.Parser Cell
cell =  Parser.space >> Parser.oneof [wall, pacman, ghost, free]

rows :: Parser.Parser Rows
rows = do Parser.newline
          r <- Parser.many cell
          if null r -- if empty
               then return []
               else do rs <- rows
                       return $ r:rs

lives :: Parser.Parser Lives
lives = Parser.many $ Parser.space >> Parser.string "PacLife" >> return Life

state :: Parser.Parser State
state = Control.Monad.liftM2 State lives rows

-- Parse .pac format
pacparse :: String -> State
pacparse s = (\[(state, str)] -> state) $ Parser.apply state s
